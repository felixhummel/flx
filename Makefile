MAKEFLAGS += --always-make

default: ruff test

ruff:
	ruff format .
	ruff check .

mypy:
	mypy .

test:
	pytest --doctest-modules

build:
	python -m build --no-isolation

clean:
	rm -rf build/ dist/

pre-release: clean ruff test build

release-test-pypi: pre-release
	twine upload --repository testpypi dist/*

release-pypi: pre-release
	twine upload --repository pypi dist/*
